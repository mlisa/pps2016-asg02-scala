import controller.MarioController;
import model.Mario;
import model.Tunnel;
import model.Block;
import model.Enemy;
import model.Coin;
import model.Character;
import model.GameElement;
import model.ElementType;

import org.junit.Test;
import utils.Res;
import view.Platform;

import static org.junit.Assert.*;

/**
 * Created by lisamazzini on 15/03/17.
 */
public class MarioControllerTest {

    private static final int MARIO_X = 0;
    private static final int MARIO_Y = 0;

    private MarioController marioController;
    private GameElement gameElementNear;
    private GameElement gameElementFar;
    private Character enemy;
    private Coin coin;

    @org.junit.Before
    public void setUp() throws Exception {
        this.marioController = new MarioController(new Mario(MARIO_X, MARIO_Y), new Platform());
        this.gameElementNear = new Tunnel(10,0);
        this.gameElementFar = new Block(100,100);
        this.enemy = new Enemy(3, 0, ElementType.MUSHROOM);
        this.coin = new Coin(0,0);
    }

    @Test
    public void contactMarioAndGameObject(){
        assertTrue(marioController.isNearby(gameElementNear));
    }

    @Test
    public void notContactMarioAndGameObject(){
        assertFalse(marioController.isNearby(gameElementFar));
    }

    @Test
    public void marioCollectsCoin(){
        System.out.println(marioController.contactPiece(this.coin));
        assertTrue(marioController.contactPiece(this.coin));
    }

    @Test
    public void marioDiesAfterTouchingEnemy(){
        marioController.contactCharacter(this.enemy);
        assertFalse(marioController.checkIfAlive());
    }

    @Test
    public void isMarioJumpingRight(){
        marioController.setMarioJumping(true);
        marioController.setMarioMovingRight(true);
        assertTrue(marioController.isMarioJumping());
        assertEquals(Res.IMG_MARIO_SUPER_DX, marioController.getMarioJumpingImage());
    }

    @Test
    public void isMarioJumpingLeft(){
        marioController.setMarioJumping(true);
        marioController.setMarioMovingRight(false);
        assertTrue(marioController.isMarioJumping());
        assertEquals(Res.IMG_MARIO_SUPER_SX, marioController.getMarioJumpingImage());
    }

}