import controller.EnemyController;
import controller.MarioController;
import model.ElementType;
import model.Mario;
import model.GameElement;
import model.Enemy;
import model.Tunnel;
import model.Block;
import org.junit.Before;
import org.junit.Test;
import view.Platform;

import static org.junit.Assert.*;

/**
 * Created by lisamazzini on 15/03/17.
 */
public class EnemyControllerTest {

    private static final int ENEMY_DYING_X = 20;
    private static final int ENEMY_DYING_Y = 70;
    private static final int ENEMY_KILLING_X = 0;
    private static final int ENEMY_KILLING_Y = 20;
    private static final int MARIO_X = 15;
    private static final int MARIO_Y = 20;

    private EnemyController enemyControllerDying;
    private EnemyController enemyControllerKilling;
    private MarioController marioController;
    private GameElement gameElementNear;
    private GameElement gameElementFar;

    @Before
    public void setUp(){
        this.enemyControllerDying = new EnemyController(new Enemy(ENEMY_DYING_X, ENEMY_DYING_Y, ElementType.TURTLE));
        this.enemyControllerKilling = new EnemyController(new Enemy(ENEMY_KILLING_X, ENEMY_KILLING_Y, ElementType.TURTLE));
        this.marioController = new MarioController(new Mario(MARIO_X, MARIO_Y), new Platform());
        this.gameElementNear = new Tunnel(10,0);
        this.gameElementFar = new Block(100,100);
    }

    @Test
    public void canMarioKillEnemy(){
        marioController.contactCharacter(this.enemyControllerDying.enemy());
        assertFalse(this.enemyControllerDying.checkIfAlive());
    }
    
    @Test
    public void canEnemyKillMario(){
        marioController.contactCharacter(this.enemyControllerKilling.enemy());
        assertFalse(this.marioController.checkIfAlive());
    }

    @Test
    public void contactEnemyAndGameObject(){
        assertTrue(enemyControllerDying.isNearby(gameElementNear));
    }

    @Test
    public void notContactEnemyAndGameObject(){
        assertFalse(enemyControllerDying.isNearby(gameElementFar));
    }
}