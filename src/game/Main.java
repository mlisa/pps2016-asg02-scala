package game;

import view.Platform;
import view.Refresh;

import javax.swing.*;

public class Main {

    private static final int WINDOW_WIDTH = 700;
    private static final int WINDOW_HEIGHT = 360;
    private static final String WINDOW_TITLE = "Super Mario";


    public static void main(String[] args) {
        JFrame mainFrame = new JFrame(WINDOW_TITLE);
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainFrame.setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
        mainFrame.setLocationRelativeTo(null);
        mainFrame.setResizable(true);
        mainFrame.setAlwaysOnTop(true);

        Platform platform = new Platform();
        mainFrame.setContentPane(platform);
        mainFrame.setVisible(true);

        Thread timer = new Thread(new Refresh(platform));
        timer.start();
    }
}
