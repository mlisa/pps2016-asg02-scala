package controller

import model.{Enemy, GameElement}


class EnemyController(var enemy: Enemy) extends CharacterController{
  val Margin = 5

  def move(speed : Int) : Unit = {
    this.enemy.offsetX = if (this.enemy.isMovingToRight) speed  else -speed
    this.enemy.x = this.enemy.x + this.enemy.offsetX
  }

  override def isNearby(element: GameElement): Boolean = this.enemy.x > element.x - enemy.ProximityMargin && this.enemy.x < element.x + element.width + enemy.ProximityMargin || this.enemy.x + this.enemy.width > element.x - enemy.ProximityMargin && this.enemy.x + this.enemy.width < element.x + element.width + enemy.ProximityMargin

  override def contact(gameElement: GameElement): Unit = checkIfContactObject(gameElement)

  override def checkIfAlive: Boolean = this.enemy.alive

  private def hitAhead(element: GameElement) = !(this.enemy.x + this.enemy.width < element.x || this.enemy.x + this.enemy.width > element.x + Margin || this.enemy.y + this.enemy.height <= element.y || this.enemy.y >= element.y + element.height)

  private def hitBack(element: GameElement) = !(this.enemy.x > element.x + element.width || this.enemy.x + this.enemy.width < element.x + element.width - Margin || this.enemy.y + this.enemy.height <= element.y || this.enemy.y >= element.y + element.height)

  private def checkIfContactObject(element: GameElement) =
    if (hitAhead(element) && this.enemy.isMovingToRight) {
    this.enemy.isMovingToRight = false
    this.enemy.offsetX = -1
  }
  else if (this.hitBack(element) && !this.enemy.isMovingToRight) {
    this.enemy.isMovingToRight = true
    this.enemy.offsetX = 1
  }
}