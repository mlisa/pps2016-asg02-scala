package controller

import view.Platform
import model.{Character, Coin, GameElement, Mario }
import utils.Res

/**
  * Created by lisamazzini on 12/03/17.
  */

class MarioController(var mario: Mario, var platform: Platform) extends CharacterController {

  var jumpingImage: String = Res.IMG_MARIO_SUPER_DX

  private val Margin = 5

  def getMarioX: Int = this.mario.x

  def getMarioY: Int = this.mario.y

  def getMarioWalkingImage(name: String, frequency: Int): String = this.mario.getWalkingImage(name, frequency)

  def setMarioMoving(moving: Boolean): Unit = this.mario.isMoving = moving

  def setMarioMovingRight(movingToRight: Boolean): Unit = this.mario.isMovingToRight = movingToRight

  def setMarioJumping(jumping: Boolean): Unit = this.mario.jumping = jumping

  override def isNearby(element: GameElement): Boolean = (this.mario.x > element.x - this.mario.ProximityMargin && this.mario.x < element.x + element.width + this.mario.ProximityMargin) || (this.mario.x + this.mario.width > element.x - this.mario.ProximityMargin && this.mario.x + this.mario.width < element.x + element.width + this.mario.ProximityMargin)

  def contactCharacter(character: Character): Unit = {
    this.makeMarioContactCharacter(character)
  }

  override def contact(gameElement: GameElement): Unit = {
    this.makeMarioContactObject(gameElement)
  }

  def contactPiece(coin: Coin): Boolean = this.hitBack(coin) || this.hitAbove(coin) || this.hitAhead(coin) || this.hitBelow(coin)

  def getMarioJumpingImage: String = {
    this.makeMarioJump()
    this.jumpingImage
  }

  def isMarioJumping: Boolean = this.mario.jumping

  override def checkIfAlive: Boolean = this.mario.alive

  private def hitAbove(element: GameElement) = !(this.mario.x + this.mario.width < element.x + Margin || this.mario.x > element.x + element.width - Margin || this.mario.y < element.y + element.height || this.mario.y > element.y + element.height + Margin)

  private def hitAhead(element: GameElement) = !(this.mario.x + this.mario.width < element.x || this.mario.x + this.mario.width > element.x + Margin || this.mario.y + this.mario.height <= element.y || this.mario.y >= element.y + element.height)

  private def hitBack(element: GameElement) = !(this.mario.x > element.x + element.width || this.mario.x + this.mario.width < element.x + element.width - Margin || this.mario.y + this.mario.height <= element.y || this.mario.y >= element.y + element.height)

  private def hitBelow(element: GameElement) = !(this.mario.x + this.mario.width < element.x || this.mario.x > element.x + element.width || this.mario.y + this.mario.height < element.y || this.mario.y + this.mario.height > element.y)

  private def makeMarioJump() = {
      this.mario.jumpingExtent = this.mario.jumpingExtent + 1
      if (this.mario.jumpingExtent < this.mario.JumpingLimit) {
        if (this.mario.y > this.platform.getHeightLimit) this.mario.y = this.mario.y - 4
        else {
          this.mario.jumpingExtent = this.mario.JumpingLimit
          this.jumpingImage = if (this.mario.isMovingToRight) Res.IMG_MARIO_SUPER_DX else Res.IMG_MARIO_SUPER_SX
        }
      }
      else if (this.mario.y + this.mario.height < this.platform.getFloorOffsetY) {
        this.mario.y = this.mario.y + 1
        this.jumpingImage = if (this.mario.isMovingToRight) Res.IMG_MARIO_SUPER_DX else Res.IMG_MARIO_SUPER_SX
      }
      else {
        this.jumpingImage = if (this.mario.isMovingToRight) Res.IMG_MARIO_ACTIVE_DX else Res.IMG_MARIO_ACTIVE_SX
        this.mario.jumping = false
        this.mario.jumpingExtent = 0
      }

  }

  private def makeMarioContactObject(gameElement: GameElement) = {
    if (this.hitAhead(gameElement) && this.mario.isMovingToRight || this.hitBack(gameElement) && !this.mario.isMovingToRight) {
      this.platform.setMov(0)
      this.mario.isMoving = false
    }
    if (this.hitBelow(gameElement) && this.mario.jumping) this.platform.setFloorOffsetY(gameElement.y)
    else if (!this.hitBelow(gameElement)) {
      this.platform.setFloorOffsetY(this.mario.FloorOffsetYInitial)
      if (!this.mario.jumping) this.mario.y = this.mario.MarioOffsetYInitial
      if (hitAbove(gameElement)) this.platform.setHeightLimit(gameElement.y + gameElement.height) // the new sky goes below the object
      else if (!this.hitAbove(gameElement) && !this.mario.jumping) this.platform.setHeightLimit(0) // initial sky
    }
  }

  private def makeMarioContactCharacter(character: Character) = {
    if (this.hitAhead(character) || this.hitBack(character)) {
      if (character.alive) {
        this.mario.isMoving = false
        this.mario.alive = false
        this.platform.endGame()
      } else this.mario.alive = true
    }
    else if (this.hitBelow(character)) {
      character.isMoving = false
      character.alive = false
    }
  }
}