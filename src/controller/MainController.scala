package controller

import game.Audio
import view.Platform
import model._
import utils.Res
import java.util

import scala.collection.mutable.ListBuffer

/**
  * Created by lisamazzini on 12/03/17.
  */

class MainController(var platform: Platform) {
  private val MARIO_X = 300
  private val MARIO_Y = 245
  private val TURTLE_X = 800
  private val TURTLE_Y = 243
  private val MUSHROOM_X = 950
  private val MUSHROOM_Y = 263
  private val SPIKED_X = 750
  private val SPIKED_Y = 263
  var speed = 1

  val marioController : MarioController = new MarioController(new Mario(MARIO_X, MARIO_Y), platform)

  val objects : ListBuffer[GameElement] = new ListBuffer[GameElement]
  var coins : ListBuffer[CoinThread] = new ListBuffer[CoinThread]
  private val enemyControllers : ListBuffer[EnemyController] = new ListBuffer[EnemyController]
  enemyControllers += EnemyFactory.createTurtle(TURTLE_X, TURTLE_Y).getEnemyController
  enemyControllers += EnemyFactory.createMushroom(MUSHROOM_X, MUSHROOM_Y).getEnemyController
  enemyControllers += EnemyFactory.createSpiked(SPIKED_X, SPIKED_Y).getEnemyController
  this.initialiseEnvironment()


  def refreshEnvironment(): Unit = {
    this.checkNearbyObjects()
    this.checkNearbyCoins()
    this.checkNearbyCharacters()
    this.makeEnemiesMove()
  }


  def getEnemies: util.ArrayList[Enemy] = {
    val enemies = new util.ArrayList[Enemy]
    for (e <- enemyControllers) {
      enemies.add( e.enemy)
    }
    enemies
  }

  private def initialiseEnvironment() = {
    this.objects.append(new Tunnel(600, 230),
                        new Tunnel(1000, 230),
                        new Tunnel(1600, 230),
                        new Tunnel(1900, 230),
                        new Tunnel(2500, 230),
                        new Tunnel(3000, 230),
                        new Tunnel(3800, 230),
                        new Tunnel(4500, 230),
                        new Block(400, 180),
                        new Block(1200, 180),
                        new Block(1270, 170),
                        new Block(1340, 160),
                        new Block(2000, 180),
                        new Block(2600, 160),
                        new Block(2650, 180),
                        new Block(3500, 160),
                        new Block(3550, 140),
                        new Block(4000, 170),
                        new Block(4200, 200),
                        new Block(4300, 210))

    this.coins.append(new CoinThread(402, 145),
                      new CoinThread(1202, 140),
                      new CoinThread(1272, 95),
                      new CoinThread(1342, 40),
                      new CoinThread(1650, 145),
                      new CoinThread(2650, 145),
                      new CoinThread(3000, 135),
                      new CoinThread(3400, 125),
                      new CoinThread(4200, 145),
                      new CoinThread(4600, 40))
  }

  private def checkNearbyObjects() = {
    for (gameElement <- this.objects) {
      if (this.marioController.isNearby(gameElement)) this.marioController.contact(gameElement)
      for (e <- enemyControllers) {
        if (e.isNearby(gameElement)) e.contact(gameElement)
      }
      this.move(gameElement)
    }
  }

  private def checkNearbyCoins() = {
    val iterator : Iterator[CoinThread] = coins.iterator
    while (iterator.hasNext) {
      var coin: CoinThread = iterator.next()
      if (this.marioController.contactPiece(coin.getCoin)) {
        Audio.playSound(Res.AUDIO_MONEY)
        this.coins -= coin
      }
      this.move(coin.getCoin)
    }
  }

  private def checkNearbyCharacters() = {
    for(i <- enemyControllers.indices) {
      for (j <- enemyControllers.indices) {
        if ( j != i ) checkContactBetweenCharacters(enemyControllers.apply(i), enemyControllers.apply(j))

      }
      checkContactBetweenCharacters(marioController, enemyControllers.apply(i))
    }
  }

  private def checkContactBetweenCharacters(a: CharacterController, b: EnemyController) = if (a.isNearby(b.enemy)) if (a.getClass == marioController.getClass) a.asInstanceOf[MarioController].contactCharacter(b.enemy)
  else a.contact(b.enemy)

  private def makeEnemiesMove() = {
    for (e <- enemyControllers) {
      e.move(this.speed)
    }
  }

  private def move(gameElement: GameElement) = if (this.platform.getxPos >= 0) gameElement.x = gameElement.x - this.platform.getMov
}