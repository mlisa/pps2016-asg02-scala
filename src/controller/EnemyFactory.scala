package controller

import model.ElementType
import model.Enemy

/**
  * Created by lisamazzini on 13/03/17.
  */
trait Factory {
  def createTurtle(x: Int, y: Int): EnemyThread

  def createMushroom(x: Int, y: Int): EnemyThread

  def createSpiked(x:Int, y:Int) : EnemyThread
}

object EnemyFactory extends Factory {
  override def createTurtle(x: Int, y: Int) = new EnemyThread(new EnemyController(new Enemy(x, y, ElementType.TURTLE)))

  override def createMushroom(x: Int, y: Int) = new EnemyThread(new EnemyController(new Enemy(x, y, ElementType.MUSHROOM)))

  override def createSpiked(x: Int, y: Int) = new EnemyThread(new EnemyController(new Enemy(x, y, ElementType.SPIKED)))
}