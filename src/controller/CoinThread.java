package controller;

import model.Coin;
import utils.Res;

public class CoinThread implements Runnable {

    private static final int PAUSE = 5;
    private static final int FLIP_FREQUENCY = 25;
    private int counter;
    private Coin coin;

    public CoinThread(int x, int y) {
        this.coin = new Coin(x,y);
    }

    public String imageOnMovement() {
        return (++this.counter % FLIP_FREQUENCY == 0 ? Res.IMG_PIECE1 : Res.IMG_PIECE2);
    }

    @Override
    public void run() {
        while (true) {
            this.imageOnMovement();
            try {
                Thread.sleep(PAUSE);
            } catch (InterruptedException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    public Coin getCoin() {
        return coin;
    }



}
