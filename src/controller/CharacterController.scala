package controller

import model.GameElement

/**
  * Created by lisamazzini on 09/04/17.
  */

trait CharacterController{
  def isNearby(pers: GameElement): Boolean

  def contact(gameElement: GameElement): Unit

  def checkIfAlive: Boolean
}