package view;

public class Refresh implements Runnable {

    private final int PAUSE = 5;
    private Platform platform;
    public Refresh(Platform platform){
        this.platform = platform;
    }

    public void run() {
        while (!this.platform.isGameOver()) {
            this.platform.repaint();
            try {
                Thread.sleep(PAUSE);
            } catch (InterruptedException e) {
                System.out.println(e.getMessage());
            }
        }
    }

} 
