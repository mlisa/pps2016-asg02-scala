package view;

import controller.MainController;
import controller.MarioController;
import game.Audio;
import game.Keyboard;
import model.*;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.util.List;

import javax.swing.JPanel;

import model.ElementType;
import utils.Res;
import utils.Utils;

@SuppressWarnings("serial")
public class Platform extends JPanel {

    private static final int MARIO_FREQUENCY = 25;
    private static final int MUSHROOM_FREQUENCY = 45;
    private static final int TURTLE_FREQUENCY = 45;
    private static final int SPIKED_FREQUENCY = 45;
    private static final int MUSHROOM_DEAD_OFFSET_Y = 20;
    private static final int TURTLE_DEAD_OFFSET_Y = 30;
    private static final int SPIKED_DEAD_OFFSET_Y = 0;
    private static final int FLAG_X_POS = 4650;
    private static final int CASTLE_X_POS = 4850;
    private static final int FLAG_Y_POS = 115;
    private static final int CASTLE_Y_POS = 145;
    private static final int BACKGROUND_WIDTH = 800;
    private static final int FRAME_WIDTH = 4600;

    private Image imgBackground1;
    private Image imgBackground2;
    private Image castle;
    private Image start;
    private int background1PosX;
    private int background2PosX;
    private int mov;
    private int xPos;
    private int floorOffsetY;
    private int heightLimit;
    private boolean gameOver;

    private Image imgFlag;
    private Image imgCastle;

    private MainController mainController;

    public Platform() {
        super();
        this.background1PosX = -50;
        this.background2PosX = 750;
        this.mov = 0;
        this.xPos = -1;
        this.floorOffsetY = 293;
        this.heightLimit = 0;
        this.imgBackground1 = Utils.getImage(Res.IMG_BACKGROUND);
        this.imgBackground2 = Utils.getImage(Res.IMG_BACKGROUND);
        this.castle = Utils.getImage(Res.IMG_CASTLE);
        this.start = Utils.getImage(Res.START_ICON);

        this.imgCastle = Utils.getImage(Res.IMG_CASTLE_FINAL);
        this.imgFlag = Utils.getImage(Res.IMG_FLAG);
        this.gameOver = false;

        this.mainController = new MainController(this);

        this.setFocusable(true);
        this.requestFocusInWindow();
        this.addKeyListener(new Keyboard(this));
    }

    public int getFloorOffsetY() {
        return floorOffsetY;
    }

    public int getHeightLimit() {
        return heightLimit;
    }

    public int getMov() {
        return mov;
    }

    public int getxPos() {
        return xPos;
    }

    public void setBackground2PosX(int background2PosX) {
        this.background2PosX = background2PosX;
    }

    public void setFloorOffsetY(int floorOffsetY) {
        this.floorOffsetY = floorOffsetY;
    }

    public void setHeightLimit(int heightLimit) {
        this.heightLimit = heightLimit;
    }

    public void setxPos(int xPos) {
        this.xPos = xPos;
    }

    public void setMov(int mov) {
        this.mov = mov;
        this.mainController.speed_$eq(mov);
    }

    public void setBackground1PosX(int x) {
        this.background1PosX = x;
    }

    public MainController getMainController() {
        return mainController;
    }

    private void updateBackgroundOnMovement() {
        if (this.xPos >= 0 && this.xPos <= FRAME_WIDTH) {
            this.xPos = this.xPos + this.mov;
            // Moving the screen to give the impression that Mario is walking
            this.background1PosX = this.background1PosX - this.mov;
            this.background2PosX = this.background2PosX - this.mov;
        }

        // Flipping between background1 and background2

        if(Math.abs(this.background1PosX+BACKGROUND_WIDTH) < this.mov){
            this.background1PosX = BACKGROUND_WIDTH;
        } else if (Math.abs(this.background2PosX+BACKGROUND_WIDTH) < this.mov){
            this.background2PosX = BACKGROUND_WIDTH;
        } else if (Math.abs(this.background1PosX-BACKGROUND_WIDTH) < this.mov){
            this.background1PosX = -BACKGROUND_WIDTH;
        } else if (Math.abs(this.background2PosX-BACKGROUND_WIDTH) < this.mov){
            this.background2PosX = -BACKGROUND_WIDTH;
        }

    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics g2 = (Graphics2D) g;

        // Moving fixed controller
        this.updateBackgroundOnMovement();

        if (this.xPos >= 0 && this.xPos <= FRAME_WIDTH) {
            mainController.refreshEnvironment();
        }

        g2.drawImage(this.imgBackground1, this.background1PosX, 0, null);
        g2.drawImage(this.imgBackground2, this.background2PosX, 0, null);
        g2.drawImage(this.castle, 10 - this.xPos, 95, null);
        g2.drawImage(this.start, 220 - this.xPos, 234, null);

        for (int i = 0; i < this.mainController.objects().size(); i++) {
            g2.drawImage(Utils.getImage(this.mainController.objects().apply(i).objectImage()), this.mainController.objects().apply(i).x(),
                    this.mainController.objects().apply(i).y(), null);
        }

        for (int i = 0; i < this.mainController.coins().size(); i++) {
            g2.drawImage(Utils.getImage(this.mainController.coins().apply(i).imageOnMovement()), this.mainController.coins().apply(i).getCoin().x(),
                    this.mainController.coins().apply(i).getCoin().y(), null);
        }

        g2.drawImage(this.imgFlag, FLAG_X_POS - this.xPos, FLAG_Y_POS, null);
        g2.drawImage(this.imgCastle, CASTLE_X_POS - this.xPos, CASTLE_Y_POS, null);

        MarioController marioController = this.mainController.marioController();
        if (marioController.isMarioJumping()) {
            g2.drawImage(Utils.getImage(marioController.getMarioJumpingImage()), marioController.getMarioX(), marioController.getMarioY(), null);
        } else {
            g2.drawImage(Utils.getImage(marioController.getMarioWalkingImage(Res.IMGP_CHARACTER_MARIO, MARIO_FREQUENCY)), marioController.getMarioX(), marioController.getMarioY(), null);
        }
        List<Enemy> enemyList = this.mainController.getEnemies();

        for(Enemy e : enemyList) {
            if (e.enemyType().equals(ElementType.MUSHROOM)) {
                if (e.alive()) {
                    g2.drawImage(Utils.getImage(e.getWalkingImage(Res.IMGP_CHARACTER_MUSHROOM, MUSHROOM_FREQUENCY)), e.x(), e.y(), null);
                } else {
                    g2.drawImage(Utils.getImage(e.getDeadImage()), e.x(), e.y() + MUSHROOM_DEAD_OFFSET_Y, null);
                }
            } else if (e.enemyType().equals(ElementType.TURTLE)) {
                if (e.alive()) {
                    g2.drawImage(Utils.getImage(e.getWalkingImage(Res.IMGP_CHARACTER_TURTLE, TURTLE_FREQUENCY)), e.x(), e.y(), null);
                } else {
                    g2.drawImage(Utils.getImage(e.getDeadImage()), e.x(), e.y() + TURTLE_DEAD_OFFSET_Y, null);
                }
            } else if (e.enemyType().equals(ElementType.SPIKED)) {
                if (e.alive()) {
                    g2.drawImage(Utils.getImage(e.getWalkingImage(Res.IMGP_CHARACTER_SPIKED, SPIKED_FREQUENCY)), e.x(), e.y(), null);
                } else {
                    g2.drawImage(Utils.getImage(e.getDeadImage()), e.x(), e.y() + SPIKED_DEAD_OFFSET_Y, null);
                }
            }
        }
    }

    public void endGame(){
        Audio.playSound("/resources/audio/gameover.wav");
        this.gameOver = true;
    }

    public Boolean isGameOver(){
        return this.gameOver;
    }
}
