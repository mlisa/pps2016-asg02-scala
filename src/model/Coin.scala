package model

/**
  * Created by lisamazzini on 09/04/17.
  */
class Coin(override var x : Int, override var y : Int) extends GameElement {
  override val width: Int = ElementType.COIN.getWidth
  override val height: Int = ElementType.COIN.getHeight
  override val objectImage: String = ElementType.COIN.getDefaultImage
}
