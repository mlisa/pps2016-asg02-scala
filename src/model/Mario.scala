package model

/**
  * Created by lisamazzini on 09/04/17.
  */
class Mario(override var x: Int, override var y : Int) extends Character {
  val MarioOffsetYInitial = 243
  val FloorOffsetYInitial = 293
  val JumpingLimit = 42
  override var isMovingToRight: Boolean = true
  override var isMoving: Boolean = false
  override var counter: Int = 0
  override val width: Int = ElementType.MARIO.getWidth
  override val height: Int = ElementType.MARIO.getHeight
  override val objectImage: String = ElementType.MARIO.getDefaultImage
  override var alive: Boolean = true
  var jumping : Boolean = false
  var jumpingExtent = 0

}