package model

/**
  * Created by lisamazzini on 09/04/17.
  */
class Block(override var x : Int, override var y : Int) extends GameElement {
  override val width: Int = ElementType.BLOCK.getWidth
  override val height: Int = ElementType.BLOCK.getHeight
  override val objectImage: String = ElementType.BLOCK.getDefaultImage
}