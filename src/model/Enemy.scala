package model

import utils.Res

/**
  * Created by lisamazzini on 09/04/17.
  */
class Enemy(override var x : Int, override var y : Int, val enemyType : ElementType) extends Character {
  override var alive: Boolean = true
  override var isMovingToRight: Boolean =  true
  override var isMoving: Boolean = true
  override var counter: Int = 0
  override val width: Int = enemyType.getWidth
  override val height: Int = enemyType.getHeight
  override val objectImage: String = enemyType.getDefaultImage
  var offsetX : Int = 1

  def getDeadImage : String = enemyType match {
    case ElementType.MUSHROOM =>
      if (this.isMovingToRight) Res.IMG_MUSHROOM_DEAD_DX
      else Res.IMG_MUSHROOM_DEAD_SX
    case ElementType.TURTLE =>
      Res.IMG_TURTLE_DEAD
    case ElementType.SPIKED =>
      Res.IMG_SPIKED_DEAD
    case _ =>
      throw new UnsupportedOperationException
  }
}