package model

/**
  * Created by lisamazzini on 09/04/17.
  */
trait GameElement {

  var x : Int
  var y : Int
  val width : Int
  val height : Int
  val objectImage : String

}