package model

import utils.Res

/**
  * Created by lisamazzini on 09/04/17.
  */
trait Character extends GameElement{

  val ProximityMargin : Int = 15
  var alive : Boolean
  var isMovingToRight : Boolean
  var isMoving : Boolean
  var counter : Int

  def getWalkingImage(name:String, frequency: Int): String = Res.IMG_BASE + name + (if (!this.isMoving || {
    this.counter += 1; this.counter
  } % frequency == 0) Res.IMGP_STATUS_ACTIVE
  else Res.IMGP_STATUS_NORMAL) + (if (this.isMovingToRight) Res.IMGP_DIRECTION_DX
  else Res.IMGP_DIRECTION_SX) + Res.IMG_EXT

}