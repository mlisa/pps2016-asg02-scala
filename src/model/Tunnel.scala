package model

/**
  * Created by lisamazzini on 09/04/17.
  */
class Tunnel(override var x : Int, override var y : Int) extends GameElement {
  override val width: Int = ElementType.TUNNEL.getWidth
  override val height: Int = ElementType.TUNNEL.getHeight
  override val objectImage: String = ElementType.TUNNEL.getDefaultImage
}